package org.kshrd;

public class Main {

    public static void main(String[] args) {

        // Declare variable
        /* DataType varName [=value]; */

        // Primitive
        int intPrimitive1 = 20;
        int intPrimitive2 = 25;

        // Wrapper Class
        Integer intWrapperClass1 = new Integer("30");
        Integer intWrapperClass2 = new Integer(35);
        Integer a = new Integer(20);

        System.out.print("Is intWrapperClass1 equal to intWrapperClass2? : "+ intWrapperClass1.equals(intWrapperClass2));
        System.out.println("Is a equal to intPrimitive1? : " + a.equals(intPrimitive1));


    }
}
